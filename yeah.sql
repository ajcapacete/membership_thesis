-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2016 at 07:21 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `accountdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `yeah`
--

CREATE TABLE IF NOT EXISTS `yeah` (
  `ID` int(2) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(30) NOT NULL,
  `LastName` varchar(30) NOT NULL,
  `MiddleName` varchar(30) NOT NULL,
  `HomeAddress` varchar(50) NOT NULL,
  `BgyNo` int(5) NOT NULL,
  `ProvincialAddress` varchar(11) NOT NULL,
  `Religion` varchar(20) NOT NULL,
  `TelephoneNo` int(11) NOT NULL,
  `Office` int(11) NOT NULL,
  `CelNo` int(11) NOT NULL,
  `Birthday` date NOT NULL,
  `Birthplace` varchar(30) NOT NULL,
  `CivilStatus` varchar(11) NOT NULL,
  `Age` int(11) NOT NULL,
  `Sex` varchar(11) NOT NULL,
  `Occupation` varchar(30) NOT NULL,
  `MonthlyIncome` int(11) NOT NULL,
  `TINNo` int(11) NOT NULL,
  `SSSorGSISNo` int(11) NOT NULL,
  `PAGIBIGNo` int(11) NOT NULL,
  `PhilHealthNo` int(11) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `NameOfSpouse` varchar(30) NOT NULL,
  `SpouseOccupation` varchar(30) NOT NULL,
  `SpouseMonthlyIncome` int(11) NOT NULL,
  `NameOfBeneficiaries` varchar(30) NOT NULL,
  `BeneficiariesBirthday` date NOT NULL,
  `BeneficiariesAge` int(11) NOT NULL,
  `Relationship` varchar(30) NOT NULL,
  `NameOfSchool` varchar(30) NOT NULL,
  `YearGraduate` date NOT NULL,
  `Course` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `yeah`
--

INSERT INTO `yeah` (`ID`, `FirstName`, `LastName`, `MiddleName`, `HomeAddress`, `BgyNo`, `ProvincialAddress`, `Religion`, `TelephoneNo`, `Office`, `CelNo`, `Birthday`, `Birthplace`, `CivilStatus`, `Age`, `Sex`, `Occupation`, `MonthlyIncome`, `TINNo`, `SSSorGSISNo`, `PAGIBIGNo`, `PhilHealthNo`, `Email`, `NameOfSpouse`, `SpouseOccupation`, `SpouseMonthlyIncome`, `NameOfBeneficiaries`, `BeneficiariesBirthday`, `BeneficiariesAge`, `Relationship`, `NameOfSchool`, `YearGraduate`, `Course`) VALUES
(1, 'jason', '123', '2123', 'male', 0, '0', '0', 0, 0, 0, '0000-00-00', '0', '0', 0, '0', '0', 0, 0, 0, 0, 0, '0', '0', '0', 0, '0', '0000-00-00', 0, '0', '0', '0000-00-00', ''),
(9, 'danver', 'olequino', 'dacillo', '34 pacheco st. balubaran valenzuela city', 1440, 'bicol', 'catholic', 4401443, 0, 2147483647, '1993-11-22', 'manila', 'single', 22, 'male', 'none', 30000, 2147483647, 0, 0, 2147483647, 'danverolequino@gmail.com', 'sti', 'tambay', 5, 'bentong', '0000-00-00', 0, 'friend', 'sti', '0000-00-00', 'bsit'),
(10, 'danver', 'olequini', 'dacillo', '34 pacheco st. balubaran valenzuela city', 1440, 'bicol', 'catholic', 4401443, 123, 2147483647, '1993-11-22', 'manila', 'single', 22, 'male', 'none', 30000, 123445678, 0, 0, 12345678, 'danverolequino@gmail.com', 'sti', 'tambay', 5, 'bentong', '0000-00-00', 0, 'friend', 'sti', '2016-10-10', 'bsit'),
(11, 'kurukong', 'ninjatus', 'purukosaytus', '213 hhehe St.', 0, 'Quezon', 'Catholic', 295812, 1234567, 2147483647, '1995-03-16', 'Manilz', '', 0, '', 'wala', 10000, 89898, 0, 0, 8878, 'jhason_smile@yahoo.com', '', 'wala', 20000, 'jsldsalk', '0000-00-00', 0, 'friend', '', '0000-00-00', ''),
(12, 'dante', 'delos', 'santos', '34 pacheco balubaran valenzuela city', 0, 'bicol', 'catholic', 4401443, 4431927, 2147483647, '1993-12-22', 'maynila', '', 0, '', 'none', 1231, 123143, 0, 0, 123145, 'danverolequino@gmail.com', 'bshrm', 'tambay', 10, 'nene', '0000-00-00', 0, 'friend', 'bshrm', '0000-00-00', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
