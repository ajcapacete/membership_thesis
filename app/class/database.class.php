<?php

require_once '../settings/app-settings.php';

class DALQueryResult
{
	private $_results = [];

	public function __construct() {}

	public function __set($var, $val) {
		$this->_results[$var] = $val;
	}

	public function __get($var) {
		if (isset($this->_results[$var])) {
			return $this->_results[$var];
		} else {
			return null;
		}
	}
}

class TDAL
{
	public function __construct() {}

	private function dbconnect() {
		$conn = mysql_connect(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD)
			or die('Could not connect to MySQL Server');

		mysql_select_db(DATABASE_NAME, $conn)
			or die('Could not select the database');

		return $conn;
	}

	public function query($sql) {
		$conn = $this->dbconnect();
		if ($conn) {
			if (DEBUG) {
				echo "Successfully connected\n";
				var_dump($conn);
			}
		}

		$res = mysql_query($sql, $conn);

		if ($res) {
			if (strpos($sql, 'SELECT') === false) {
				return true;
			}
		} else {
			if (strpos($sql, 'SELECT') === false) {
				return false;
			} else {
				return null;
			}
		}

		$results = [];

		while ($row = mysql_fetch_array($res, MYSQL_ASSOC)) {
			if (DEBUG) {
				echo 'LINE:' . __LINE__ + 1 . "\n";
				print_r($row);
			}
			$result = new DALQueryResult();

			foreach ($row as $k => $v) {
				$result->k = $v;
			}

			$results[] = $result;
		}

		return $results;
	}
}

class MembersDAL extends TDAL {
	public function test_select() {
		$sql = "SELECT * FROM yeah";

		return $this->query($sql);
	}
}


// test
if (DEBUG) {
	$membersDAL = new MembersDAL();

	$results = $membersDAL->test_select();

	if ($results) {
		echo "\n------------------------------------------------------------\n";
		foreach ($results as $result) {
			var_dump($result);
		}
	} else {
		echo "\n" . 'No information found';
	}
}