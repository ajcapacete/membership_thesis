<?php

interface IRecord
{
	public function get($field);
	public function set($field, $val);
	public function get_json();
}

class TMember implements IRecord
{
	// $fname, $lname, $mname, $Haddress, $BgyNo, $PA, $Religion, $Telno, $Office,
	// 	$CelNo, $BDay, $BPlace, $CS, $Age, $Sex, $Occ, $MI, $TinNo, $SSSorGSISNo,
	// 	$PINo, $PhilNo, $email, $NOS, $SO, $SMI, $NOB, $BB, $BA, $Rel, $NOS, $YG,
	// 	$Course;

	private $data = [];

	public function __construct($data) {
		foreach ($data as $key => $value) {
			$this->set($key, $value);
		}
	}

	public function get($field) {
		return $this->data[$field];
	}

	public function set($field, $val) {
		$clean_val = strip_tags($val);
		$clean_val = htmlspecialchars($val);

		$this->data[$field] = $clean_val;
	}

	public function get_json() {
		return json_encode($this->data);
	}
}

function display(IRecord $r) {
	echo $r->get('name');
	echo '----';
	echo $r->get_json();
}

display(new TMember([
	'name'  => '<strong>qwerty</strong><script>alert();</script>',
	'age'   => 43]));