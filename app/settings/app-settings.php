<?php

define('DATABASE_HOST', 'localhost');
define('DATABASE_NAME', 'accountdb');
define('DATABASE_USERNAME', 'root');
define('DATABASE_PASSWORD', '');
define('DATABASE_PORT', '');
define('DATABASE_SOCKET', '');

define('APP_DIRECTORY', 'app');
define('TEMPLATE_DIRECTORY', APP_DIRECTORY . '/templates');

define('DEBUG', true);